import numpy as np
import calfem.core as cfc
import von_mises_data as dat
import truss_structure as trs


def bar2Ne(ex, ey, ep, qe):
    """
    Compute the normal force for two dimensional bar element. (10-33)

    :param list ex: element x coordinates [x1, x2]
    :param list ey: element y coordinates [y1, y2]
    :param list ep: [E, A]: E - Young's modulus, A - Cross section area    
    :return normal force
    """
    EA = ep[0]*ep[1]

    b = np.mat([[ex[1]-ex[0]], [ey[1]-ey[0]]])
    L = np.sqrt(b.T*b)
    L = L.item()

    B0 = np.array([-1., 0., 1., 0.])/L

    M = np.array([[1., 0., -1., 0.],
                  [0., 1., 0., -1.],
                  [-1., 0., 1., 0.],
                  [0., -1., 0., 1.]
                  ])/L**2

    B1 = np.matmul(qe.T, M)
    B = B0+0.5*B1

    return EA*np.matmul(B, qe)


def bar2Fe(ex, ey, qe, Ne):
    """
    Compute the Fe matrix for two dimensional bar element. (10-43c)

    :param list ex: element x coordinates [x1, x2]
    :param list ey: element y coordinates [y1, y2]
    :param list qe: element global displacements [q1, q2, q3, q4]
    :param list Ne: element normal force [Ne]
    :return mat Fe: [4 x 1]
    """
    b = np.mat([[ex[1]-ex[0]], [ey[1]-ey[0]]])
    L = np.sqrt(b.T*b)
    L = L.item()

    du = qe[2] - qe[0]
    dv = qe[3] - qe[1]

    Fe = np.array([
        [-1-du/L],
        [-dv/L],
        [1+du/L],
        [dv/L]
    ])

    return Ne*Fe


def bar2ke_0(ex, ey, ep):
    """
    Compute the element stiffness matrix for two dimensional bar element in global coord. system. (10-36)

    :param list ex: element x coordinates [x1, x2]
    :param list ey: element y coordinates [y1, y2]
    :param list ep: [E, A]: E - Young's modulus, A - Cross section area
    :return mat Ke_0: stiffness matrix, [4 x 4]
    """
    EA = ep[0]*ep[1]

    b = np.mat([
        [ex[1]-ex[0]],
        [ey[1]-ey[0]]
    ])
    L = np.sqrt(b.T*b)
    L = L.item()

    Ke_0 = (EA/L)*np.mat([[1., 0., -1., 0.],
                          [0., 0.,  0., 0.],
                          [-1., 0., 1., 0.],
                          [0.,  0.,  0., 0]
                          ])

    n = np.asarray(b.T/L).reshape(2,)

    G = np.mat([
        [n[0],  n[1],   0.,   0.],
        [-n[1], n[0],   0.,   0.],
        [0.,    0.,   n[0], n[1]],
        [0.,    0.,  -n[1], n[0]]
    ])

    return G.T*Ke_0*G


def bar2ke_u(ex, ey, ep, qe):
    """
    Compute the element displacement matrix for two dimensional bar element in global coord. system. (10-36)

    :param list ex: element x coordinates [x1, x2]
    :param list ey: element y coordinates [y1, y2]
    :param list ep: [E, A]: E - Young's modulus, A - Cross section area
    :param list qe: element global displacements [q1, q2, q3, q4]
    :return mat Ke_u: displacement matrix [4 x 4]
    """

    EA = ep[0]*ep[1]

    b = np.mat([
        [ex[1]-ex[0]],
        [ey[1]-ey[0]]
    ])
    L = np.sqrt(b.T*b)
    L = L.item()

    du = qe[2] - qe[0]
    dv = qe[3] - qe[1]

    Ke_1 = (3/2)*(EA/L**2)*np.mat([[2*du, dv, -2*du, -dv],
                                   [dv,    0,  -dv,    0],
                                   [-2*du, -dv, 2*du, dv],
                                   [-dv,   0,   dv,    0]
                                   ])

    Ke_2 = (3/2)*(EA/L**3)*np.mat([[du**2, du*dv, -du**2, -du*dv],
                                   [du*dv, dv**2, -du*dv, -dv**2],
                                   [-du**2, -du*dv, du**2, du*dv],
                                   [-du*dv, -dv**2, du*dv, dv**2]
                                   ])

    n = np.asarray(b.T/L).reshape(2,)

    G = np.mat([
        [n[0],  n[1],   0.,   0.],
        [-n[1], n[0],   0.,   0.],
        [0.,    0.,   n[0], n[1]],
        [0.,    0.,  -n[1], n[0]]
    ])

    Ke_u = (2/3)*Ke_1 + (2/3)*Ke_2

    return G.T*Ke_u*G


def bar2ke_sigma(ex, ey, Ne):
    """
    Compute element stress matrix for two dimensional bar element in global coord. system. (10-36)

    :param list ex: element x coordinates [x1, x2]
    :param list ey: element y coordinates [y1, y2]
    :param list Ne: element normal force [Ne]
    :return mat Ke: stress matrix [4 x 4]
    """

    b = np.mat([
        [ex[1]-ex[0]],
        [ey[1]-ey[0]]
    ])
    L = np.sqrt(b.T*b)
    L = L.item()

    Ke_sigma = (Ne/L)*np.mat([
        [1, 0, -1, 0],
        [0, 0, 0, 0],
        [-1, 0, 1, 0],
        [0, 0, 0, 0]])

    n = np.asarray(b.T/L).reshape(2,)

    G = np.mat([
        [n[0],  n[1],   0.,   0.],
        [-n[1], n[0],   0.,   0.],
        [0.,    0.,   n[0], n[1]],
        [0.,    0.,  -n[1], n[0]]
    ])

    return G.T*Ke_sigma*G


def l_elem(ex, ey):
    """
    Compute the length L bar elements.

    :param list ex: element x coordinates [x1, x2]
    :param list ey: element y coordinates [y1, y2]
    :return L
    """

    b = np.mat([[ex[1]-ex[0]], [ey[1]-ey[0]]])
    L = np.sqrt(b.T*b)
    L = L.item()

    return L


# Wczytywanie danych

coords = dat.coords
elems = dat.elems
supports = dat.supports
H = dat.H
sb0 = dat.sb0

truss1 = trs.Truss(coords, elems, supports)

n_num = truss1.nodes_number()
e_num = truss1.element_number()
e_coords = truss1.element_coords()
ex = truss1.ex()
ey = truss1.ey()

# Dane materialowe
E = 1
A = 1

# Macierz wlasciwosci materialu
ep = np.array([E, A])

# Macierz globanych stopni swobody
edof = truss1.edof().astype(int)

print("\n\n")
print("Macierz stopni swobody = \n", edof)

# Ilość stopni swobody
max_edof = np.amax(edof)

# Długości elementów
L = np.zeros((e_num))

for l in range(0, e_num):
    L[l] = l_elem(ex[l], ey[l])

print("\n\n")
print("L = \n", L)


# Macierz obciazen
# f_final = -100      # [N]

Q_final = np.zeros((max_edof, 1))
Q_final[3] = -1/(E*A*sb0**3)
Q = np.zeros((max_edof, 1))

# Przyrosty
deltaQ = [0.1, 0.1, 0.1, 0.08]

# Warunki brzegowe
bc = np.array([1, 2, 5, 6])

# Przygotowanie macierzy sztywnosci
K_0 = np.zeros((max_edof, max_edof))
K_u = np.zeros((max_edof, max_edof))
K_sigma = np.zeros((max_edof, max_edof))

Ke_0 = np.zeros((e_num, 4, 4))
Ke_u = np.zeros((e_num, 4, 4))
Ke_sigma = np.zeros((e_num, 4, 4))

q = np.asmatrix(np.zeros((max_edof, 1)))
eta = 0
N_e = np.zeros((e_num, 1))
F_e = np.zeros((e_num, 4, 1))
F = np.zeros((max_edof, 1))

# Obliczenie macierzy sztywnosci początkowej

for i in range(0, e_num):
    Ke_0[i] = bar2ke_0(ex[i], ey[i], ep)
    K_0 = cfc.assem(edof[i, :], K_0, Ke_0[i])

print("\n\n")
print("Macierz sztywności = \n", K_0)


# Pętla N-R

for m in range(0, len(deltaQ)):
    print("Krok = ", m)
    itr = 0
    dQ = deltaQ[m]*Q_final
    Q += dQ
    KT = K_0 + K_u + K_sigma
    dq, dR = cfc.solveq(KT, dQ, bc)
    q += dq
    qe = cfc.extractEldisp(edof, q)
    print("q = \n", q)
    while True:
        itr += 1
        print("Iteracja = ", itr)

        for i in range(0, e_num):
            N_e[i] = bar2Ne(ex[i], ey[i], ep, qe[i])

        for i in range(0, e_num):
            F_e[i] = bar2Fe(ex[i], ey[i], qe[i], N_e[i].item())

        # Macierz przemieszczeniowa
        for i in range(0, e_num):
            Ke_u[i] = bar2ke_u(ex[i], ey[i], ep, qe[i])
            K_u = cfc.assem(edof[i, :], K_u, Ke_u[i])

        # Macierz naprezen
        for i in range(0, e_num):
            Ke_sigma[i] = bar2ke_sigma(ex[i], ey[i], N_e[i].item())
            K_sigma, Res = cfc.assem(
                edof[i, :], K_sigma, Ke_sigma[i], F, F_e[i])

        KT = K_0 + K_u + K_sigma
        print("Ne = \n", N_e)
        Res = Q - F + dQ
        Res_norm = np.linalg.norm(Res)
        print("Res_norm = \n", Res_norm)

        dq, dR = cfc.solveq(KT, Res, bc)
        q += dq
        qe = cfc.extractEldisp(edof, q)
        eta = q[3]/H

        if itr == 1:
            break
    print("eta = ", eta)
