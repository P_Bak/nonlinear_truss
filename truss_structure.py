import numpy as np


class Truss(object):
    def __init__(self, coords, elems, supports):
        self._coords = coords
        self._elems = elems
        self._supports = supports
        self.nodes_number()
        self.element_number()
        self.e_coords = self.element_coords()
        self.axis = [(self.min_coords()), (self.max_coords()),
                     (self.min_coords()), (self.max_coords())]
        self.start_point = self.start_points()
        self.end_point = self.end_points()
        self.n_index = self.nodes_index()

    def nodes_number(self):

        shape = np.shape(self._coords)
        self.n_num = shape[0]
        return self.n_num

    def element_number(self):

        shape = np.shape(self._elems)
        self.e_num = shape[0]
        return self.e_num

    def nodes_index(self):
        n_index = np.zeros((self.n_num, 2))
        h = 0
        for k in range(0, self.n_num):
            n_index[k, :] = h+1, h+2
            h += 2
        return n_index

    def edof(self):
        edof = np.zeros((self.e_num, 2, 2))
        for k in range(0, self.e_num):
            edof[k] = np.array(
                [self.n_index[self._elems[k, 0]], self.n_index[self._elems[k, 1]]])
        return edof.reshape(self.e_num, 4)

    def element_coords(self):
        elem_coords = np.zeros((self.e_num, 2, 2))
        for k in range(0, self.e_num):
            elem_coords[k] = np.array(
                [self._coords[self._elems[k, 0]], self._coords[self._elems[k, 1]]])
        return elem_coords

    def ex(self):
        ex = np.zeros((self.e_num, 2))
        for k in range(0, self.e_num):
            ex[k, 0] = self.start_point[k, 0]
            ex[k, 1] = self.end_point[k, 0]
        return ex

    def ey(self):
        ey = np.zeros((self.e_num, 2))
        for k in range(0, self.e_num):
            ey[k, 0] = self.start_point[k, 1]
            ey[k, 1] = self.end_point[k, 1]
        return ey

    def start_points(self):
        return self.e_coords[:, 0]

    def end_points(self):
        return self.e_coords[:, 1]

    def bars_center(self):
        return 0.5*(self.start_points() + self.end_points())

    def max_coords(self):
        return np.amax(self._coords)

    def min_coords(self):
        return np.amin(self._coords)
