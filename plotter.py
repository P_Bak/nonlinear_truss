import matplotlib.pyplot as plt


class TrussPlot(object):
    def __init__(self, coords, elems, n_num, e_num, start_points, end_points, bars_center, axis, supports):
        self.nodes_x = coords[0:n_num, 0]
        self.nodes_y = coords[0:n_num, 1]
        self.n_num = n_num
        self.e_num = e_num
        self.start_points = start_points
        self.end_points = end_points
        self.bars_center = bars_center
        self.supports = supports
        self.n_index = []
        for i in range(0, n_num):
            self.n_index.append(i)

        self.e_index = []
        for j in range(0, e_num):
            self.e_index.append(j)

        self.nodes_plot()
        self.bars_plot()
        self.supports_plot()
        self.ax.set(xlim=(axis[0]-2, axis[1]+2), ylim=(axis[2]-2, axis[3]+2))
        plt.show()

    def nodes_plot(self):
        self.fig, self.ax = plt.subplots()
        self.ax.plot(self.nodes_x, self.nodes_y, 'ro', markersize=7)

        for x, y, idx in zip(self.nodes_x, self.nodes_y, self.n_index):
            self.ax.annotate('{}'.format(idx+1), xy=(x, y), xytext=(
                10, 3), textcoords='offset points', color='red')

    def bars_plot(self):
        for k in range(0, self.e_num):
            bars_x = self.start_points[k, 0], self.end_points[k, 0]
            bars_y = self.start_points[k, 1], self.end_points[k, 1]
            plt.plot(bars_x, bars_y, 'k')
        self.bars_numbers_plot()

    def bars_numbers_plot(self):
        for m in range(0, self.e_num):
            bars_cx = self.bars_center[m, 0]
            bars_cy = self.bars_center[m, 1]
            plt.plot(bars_cx, bars_cy, 'b,')
            self.ax.annotate('{}'.format(self.e_index[m]+1), xy=(
                bars_cx, bars_cy), color='blue', bbox=dict(boxstyle="round", fc="w"))

    def supports_plot(self):
        for sup in self.supports:

            sup_x = self.nodes_x[sup]
            sup_y = self.nodes_y[sup]
            plt.plot(sup_x, sup_y, color='green', marker=6, markersize=11)
