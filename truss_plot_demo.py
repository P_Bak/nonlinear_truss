import von_mises_data as dat
import truss_structure as trs
import plotter

coords = dat.coords
elems = dat.elems
supports = dat.supports

truss1 = trs.Truss(coords, elems, supports)

n_num = truss1.nodes_number()
e_num = truss1.element_number()
e_coords = truss1.element_coords()
start_points = truss1.start_points()
end_points = truss1.end_points()
bars_center = truss1.bars_center()

axis = truss1.axis

plotter.TrussPlot(coords, elems, n_num, e_num, start_points,
                  end_points, bars_center, axis, supports)
