import numpy as np

L = 1.0
H = 0.2
Px = 1.0
Pz = 1.0
E = 1.0
A = 1.0
L0 = np.sqrt(L**2 + H**2)
sb0 = H/L0

coords = np.array([
    [-L, 0.0],
    [0.0, H],
    [L, 0.0]
])


elems = np.array([
    [0, 1],
    [1, 2]
])

supports = [0, 2]


if __name__ == "__main__":
    print("Import this data into demo")
